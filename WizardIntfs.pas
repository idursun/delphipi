{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}

unit WizardIntfs;

interface

uses 
  System.Classes, Vcl.ActnList;

type
   TWizardButtonType = (wbtNone, wbtNext, wbtBack, wbtHelp);

   IWizard = interface
     ['{82A45FE8-CCA1-4DED-91DB-67F1D2989D56}']
     procedure UpdateInterface;
     function GetAction(buttonType: TWizardButtonType): TAction;
     procedure SetHeader(const header: string);
     procedure SetDescription(const desc: string);
     function GetState(const key: string):TObject;
    procedure SetState(const key: string; const value: TObject);
   end;

implementation

end.
