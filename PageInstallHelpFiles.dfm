inherited InstallHelpFilesPage: TInstallHelpFilesPage
  Left = 202
  Top = 166
  Caption = 'InstallHelpFilesPage'
  ClientHeight = 240
  ClientWidth = 412
  OnCreate = FormCreate
  ExplicitWidth = 412
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 484
    Height = 26
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      'DelphiPI has found some help files in the folder that you have s' +
      'elected. Would you like to install them as well?'
    WordWrap = True
  end
  object helpFileList: TListView [1]
    Left = 8
    Top = 50
    Width = 396
    Height = 159
    Anchors = [akLeft, akTop, akRight, akBottom]
    Checkboxes = True
    Columns = <
      item
        AutoSize = True
        Caption = 'Help File Name'
        MinWidth = 200
      end>
    GridLines = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    ViewStyle = vsReport
  end
  object btnInstallHelpFiles: TButton [2]
    Left = 290
    Top = 215
    Width = 114
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Install Help Files'
    TabOrder = 1
    OnClick = btnInstallHelpFilesClick
  end
  inherited VirtualImageList: TVirtualImageList
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'folder'
        Name = 'folder'
      end
      item
        CollectionIndex = 1
        CollectionName = 'package'
        Name = 'package'
      end
      item
        CollectionIndex = 2
        CollectionName = 'control-tree'
        Name = 'control-tree'
      end
      item
        CollectionIndex = 3
        CollectionName = 'control-tree-list'
        Name = 'control-tree-list'
      end
      item
        CollectionIndex = 4
        CollectionName = 'software'
        Name = 'software'
      end
      item
        CollectionIndex = 5
        CollectionName = 'folder-add'
        Name = 'folder-add'
      end
      item
        CollectionIndex = 6
        CollectionName = 'package-add'
        Name = 'package-add'
      end
      item
        CollectionIndex = 7
        CollectionName = 'symbol-update'
        Name = 'symbol-update'
      end
      item
        CollectionIndex = 8
        CollectionName = 'control-check-box'
        Name = 'control-check-box'
      end
      item
        CollectionIndex = 9
        CollectionName = 'control-check-box-empty'
        Name = 'control-check-box-empty'
      end>
  end
end
