object frmWizard: TfrmWizard
  Left = 468
  Top = 288
  Caption = 'Delphi PI'
  ClientHeight = 511
  ClientWidth = 634
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object HeaderPanel: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 60
    Align = alTop
    BevelEdges = [beBottom]
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object LogoImage: TImage
      Left = 4
      Top = 4
      Width = 48
      Height = 48
      Center = True
    end
    object lblHeader: TLabel
      Left = 58
      Top = 10
      Width = 61
      Height = 16
      Caption = 'lblHeader'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDescription: TLabel
      Left = 58
      Top = 32
      Width = 63
      Height = 13
      Caption = 'lblDescription'
    end
  end
  object DockPanel: TPanel
    Left = 0
    Top = 60
    Width = 634
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
  end
  object pBottom: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 466
    Width = 628
    Height = 42
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 1
    TabOrder = 2
    object Bevel1: TBevel
      AlignWithMargins = True
      Left = 1
      Top = 1
      Width = 626
      Height = 2
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Align = alTop
      Shape = bsTopLine
      ExplicitLeft = 3
      ExplicitTop = 0
      ExplicitWidth = 586
    end
    object btnBack: TButton
      AlignWithMargins = True
      Left = 446
      Top = 11
      Width = 86
      Height = 25
      Margins.Top = 5
      Margins.Bottom = 5
      Action = actBack
      Align = alRight
      TabOrder = 0
    end
    object btnNext: TButton
      AlignWithMargins = True
      Left = 538
      Top = 11
      Width = 86
      Height = 25
      Margins.Top = 5
      Margins.Bottom = 5
      Action = actNext
      Align = alRight
      Default = True
      TabOrder = 1
    end
    object btnAbout: TButton
      AlignWithMargins = True
      Left = 4
      Top = 11
      Width = 86
      Height = 25
      Margins.Top = 5
      Margins.Bottom = 5
      Action = actAbout
      Align = alLeft
      TabOrder = 2
    end
  end
  object actionList: TActionList
    Left = 424
    Top = 92
    object actNext: TAction
      Caption = 'Next >>'
      OnExecute = actNextExecute
    end
    object actBack: TAction
      Caption = '<< Back'
      OnExecute = actBackExecute
    end
    object actAbout: TAction
      Caption = 'About...'
      OnExecute = actAboutExecute
    end
  end
end
