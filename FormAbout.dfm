object frmAbout: TfrmAbout
  Left = 654
  Top = 317
  BorderStyle = bsDialog
  Caption = 'About DelphiPI'
  ClientHeight = 186
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object grpInformation: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 371
    Height = 132
    Align = alClient
    Caption = 'Information'
    TabOrder = 0
    object Label7: TLabel
      Left = 52
      Top = 28
      Width = 232
      Height = 23
      Caption = 'Delphi Package Installer'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Image2: TImage
      Left = 11
      Top = 23
      Width = 35
      Height = 35
      Center = True
    end
    object lblVersion: TLabel
      Left = 101
      Top = 61
      Width = 182
      Height = 16
      Caption = 'Utils.Version Utils.CodeName'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 11
      Top = 64
      Width = 39
      Height = 13
      Caption = 'Version:'
    end
    object Label10: TLabel
      Left = 11
      Top = 83
      Width = 80
      Height = 13
      Caption = 'Project Website:'
    end
    object Label11: TLabel
      Left = 11
      Top = 102
      Width = 73
      Height = 13
      Caption = 'Author'#39's name:'
    end
    object lblAuthorsWebsite: TLabel
      Left = 101
      Top = 121
      Width = 118
      Height = 13
      Cursor = crHandPoint
      Caption = 'http://www.idursun.com'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
      OnClick = lblAuthorsWebsiteClick
    end
    object lblProjectWebsite: TLabel
      Left = 101
      Top = 83
      Width = 232
      Height = 13
      Cursor = crHandPoint
      Caption = 'https://bitbucket.org/idursun/delphipi/wiki/Home'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = lblProjectWebsiteClick
    end
    object Label2: TLabel
      Left = 11
      Top = 121
      Width = 84
      Height = 13
      Caption = 'Author'#39's website:'
      Visible = False
    end
    object lblAuthor: TLabel
      Left = 101
      Top = 102
      Width = 57
      Height = 13
      Caption = 'Utils.Author'
    end
  end
  object pnlBottom: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 141
    Width = 371
    Height = 42
    Align = alBottom
    BevelEdges = [beTop]
    BevelOuter = bvNone
    BorderWidth = 1
    TabOrder = 1
    object Bevel1: TBevel
      AlignWithMargins = True
      Left = 1
      Top = 1
      Width = 369
      Height = 2
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Align = alTop
      Shape = bsTopLine
      ExplicitLeft = 4
    end
    object btnDonate: TButton
      AlignWithMargins = True
      Left = 4
      Top = 11
      Width = 86
      Height = 25
      Margins.Top = 5
      Margins.Bottom = 5
      Align = alLeft
      Caption = 'Donate'
      TabOrder = 0
      Visible = False
      OnClick = btnDonateClick
    end
    object btnClose: TButton
      AlignWithMargins = True
      Left = 281
      Top = 11
      Width = 86
      Height = 25
      Margins.Top = 5
      Margins.Bottom = 5
      Align = alRight
      Cancel = True
      Caption = 'Close'
      TabOrder = 1
      OnClick = btnCloseClick
    end
  end
end
