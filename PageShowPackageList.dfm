inherited ShowPackageListPage: TShowPackageListPage
  Left = 352
  Top = 188
  ActiveControl = toolbar
  Caption = 'ShowPackageListPage'
  ClientHeight = 266
  ClientWidth = 461
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 461
  ExplicitHeight = 266
  PixelsPerInch = 96
  TextHeight = 13
  object lblWait: TLabel [0]
    Left = 0
    Top = 31
    Width = 461
    Height = 201
    Align = alClient
    Alignment = taCenter
    AutoSize = False
    Caption = 'Please wait while searching folders '
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = False
    Layout = tlCenter
    ExplicitTop = 22
    ExplicitWidth = 588
    ExplicitHeight = 315
  end
  object fPackageTree: TVirtualStringTree [1]
    Left = 0
    Top = 31
    Width = 461
    Height = 201
    Align = alClient
    Colors.BorderColor = 15987699
    Colors.DisabledColor = clGray
    Colors.DropMarkColor = 15385233
    Colors.DropTargetColor = 15385233
    Colors.DropTargetBorderColor = 15385233
    Colors.FocusedSelectionColor = 15385233
    Colors.FocusedSelectionBorderColor = 15385233
    Colors.GridLineColor = 15987699
    Colors.HeaderHotColor = clBlack
    Colors.HotColor = clBlack
    Colors.SelectionRectangleBlendColor = 15385233
    Colors.SelectionRectangleBorderColor = 15385233
    Colors.SelectionTextColor = clBlack
    Colors.TreeLineColor = 9471874
    Colors.UnfocusedColor = 15987699
    Header.AutoSizeIndex = 0
    Header.Options = [hoColumnResize, hoDrag, hoVisible, hoAutoSpring]
    HintMode = hmHint
    Images = VirtualImageList
    ParentShowHint = False
    PopupMenu = pmSelectPopupMenu
    ShowHint = True
    TabOrder = 0
    TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScrollOnExpand, toAutoSpanColumns, toAutoTristateTracking, toAutoDeleteMovedNodes]
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toCheckSupport, toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning]
    TreeOptions.SelectionOptions = [toDisableDrawSelection, toFullRowSelect, toMultiSelect]
    TreeOptions.StringOptions = [toSaveCaptions]
    OnChecked = packageTreeChecked
    OnGetText = fPackageTreeGetText
    OnPaintText = fPackageTreePaintText
    OnGetImageIndex = packageTreeGetImageIndex
    OnGetHint = fPackageTreeGetHint
    OnGetNodeDataSize = packageTreeGetNodeDataSize
    OnInitChildren = fPackageTreeInitChildren
    OnInitNode = fPackageTreeInitNode
    OnKeyAction = fPackageTreeKeyAction
    Touch.InteractiveGestures = [igPan, igPressAndTap]
    Touch.InteractiveGestureOptions = [igoPanSingleFingerHorizontal, igoPanSingleFingerVertical, igoPanInertia, igoPanGutter, igoParentPassthrough]
    ExplicitTop = 69
    ExplicitHeight = 163
    Columns = <
      item
        Position = 0
        Text = 'Package'
        Width = 308
      end
      item
        Position = 1
        Text = 'Description'
        Width = 270
      end
      item
        Position = 2
        Text = 'Type'
        Width = 70
      end>
  end
  object toolbar: TToolBar [2]
    AlignWithMargins = True
    Left = 3
    Top = 6
    Width = 455
    Height = 22
    Margins.Top = 6
    AutoSize = True
    Caption = 'toolbar'
    Images = VirtualImageList
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object btnFolderView: TToolButton
      Left = 0
      Top = 0
      Action = actChangeViewToTree
    end
    object btnChangeToListView: TToolButton
      Left = 23
      Top = 0
      Action = actChangeViewToList
    end
    object btnViewDelphiVersion: TToolButton
      Left = 46
      Top = 0
      Action = actChangeViewToDelphiVersion
    end
    object sepearator1: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'sepearator1'
      ImageIndex = 2
      ImageName = 'control-tree'
      Style = tbsSeparator
    end
    object btnAddFolder: TToolButton
      Left = 77
      Top = 0
      Action = actAddPackagesFromFolder
    end
    object btnAddPackage: TToolButton
      Left = 100
      Top = 0
      Hint = 'Add package'
      Action = actAddPackages
      Caption = 'Add Package'
    end
    object btnRefresh: TToolButton
      Left = 123
      Top = 0
      Action = actRefresh
    end
    object seperator2: TToolButton
      Left = 146
      Top = 0
      Width = 8
      Caption = 'seperator2'
      ImageName = 'software-box'
      Style = tbsSeparator
    end
    object btnSelectMatching: TToolButton
      Left = 154
      Top = 0
      Action = actSelectMatching
    end
    object btnUnselectMatching: TToolButton
      Left = 177
      Top = 0
      Action = actUnselectMatching
    end
  end
  object panelCheckBox: TPanel [3]
    Left = 0
    Top = 232
    Width = 461
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object chkDependency: TCheckBox
      Left = 337
      Top = 0
      Width = 124
      Height = 34
      Align = alRight
      Caption = 'Dependant packages'
      TabOrder = 0
      OnClick = chkDependencyClick
    end
  end
  inherited VirtualImageList: TVirtualImageList
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'folder'
        Name = 'folder'
      end
      item
        CollectionIndex = 1
        CollectionName = 'package'
        Name = 'package'
      end
      item
        CollectionIndex = 2
        CollectionName = 'control-tree'
        Name = 'control-tree'
      end
      item
        CollectionIndex = 3
        CollectionName = 'control-tree-list'
        Name = 'control-tree-list'
      end
      item
        CollectionIndex = 4
        CollectionName = 'software'
        Name = 'software'
      end
      item
        CollectionIndex = 5
        CollectionName = 'folder-add-filled'
        Name = 'folder-add-filled'
      end
      item
        CollectionIndex = 6
        CollectionName = 'package-add-filled'
        Name = 'package-add-filled'
      end
      item
        CollectionIndex = 7
        CollectionName = 'symbol-update'
        Name = 'symbol-update'
      end
      item
        CollectionIndex = 8
        CollectionName = 'control-check-box'
        Name = 'control-check-box'
      end
      item
        CollectionIndex = 9
        CollectionName = 'control-check-box-empty'
        Name = 'control-check-box-empty'
      end>
  end
  object pmSelectPopupMenu: TPopupMenu
    Left = 160
    Top = 104
    object miSelectMatching: TMenuItem
      Action = actSelectMatching
    end
    object miUnselectMatching: TMenuItem
      Action = actUnselectMatching
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Remove1: TMenuItem
      Action = actRemove
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object miSelectAll: TMenuItem
      Action = actSelectAll
    end
    object miUnselectAll: TMenuItem
      Action = actUnselectAll
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object miCollapse: TMenuItem
      Action = actCollapse
    end
    object miExpand: TMenuItem
      Action = actExpand
    end
  end
  object ActionList: TActionList
    Images = VirtualImageList
    Left = 56
    Top = 104
    object actRemove: TAction
      Category = 'Package List'
      Caption = 'Remove'
      ShortCut = 46
      OnExecute = actRemoveExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actChangeViewToTree: TAction
      Category = 'View'
      AutoCheck = True
      Checked = True
      GroupIndex = 1
      Hint = 'Switch to Tree View '
      ImageIndex = 2
      ImageName = 'control-tree'
      OnExecute = actChangeViewToTreeExecute
    end
    object actChangeViewToList: TAction
      Category = 'View'
      AutoCheck = True
      GroupIndex = 1
      Hint = 'Switch to List View '
      ImageIndex = 3
      ImageName = 'control-tree-list'
      OnExecute = actChangeViewToListExecute
    end
    object actSelectAll: TAction
      Category = 'Package List'
      Caption = 'Select All'
      Hint = 'Select All'
      OnExecute = actSelectAllExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actUnselectAll: TAction
      Category = 'Package List'
      Caption = 'Unselect All'
      Hint = 'Unselect All'
      OnExecute = actUnselectAllExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actSelectMatching: TAction
      Category = 'Package List'
      Caption = 'Select matching...'
      Hint = 'Select matching...'
      ImageIndex = 8
      ImageName = 'control-check-box'
      OnExecute = actSelectMatchingExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actUnselectMatching: TAction
      Category = 'Package List'
      Hint = 'Unselect matching...'
      Caption = 'Unselect matching...'
      ImageIndex = 9
      ImageName = 'control-check-box-empty'
      OnExecute = actUnselectMatchingExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actExpand: TAction
      Category = 'Package List'
      Caption = 'Expand'
      OnExecute = actExpandExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actCollapse: TAction
      Category = 'Package List'
      Caption = 'Collapse'
      OnExecute = actCollapseExecute
      OnUpdate = PackageListActionsUpdate
    end
    object actAddPackagesFromFolder: TAction
      Hint = 'Add packages from a folder'
      ImageIndex = 5
      ImageName = 'folder-add-filled'
      OnExecute = actAddPackagesFromFolderExecute
    end
    object actAddPackages: TAction
      Hint = 'Add Packages'
      ImageIndex = 6
      ImageName = 'package-add-filled'
      OnExecute = actAddPackagesExecute
    end
    object actChangeViewToDelphiVersion: TAction
      Category = 'View'
      AutoCheck = True
      GroupIndex = 1
      Hint = 'Switch to Delphi Version View'
      ImageIndex = 4
      ImageName = 'software'
      OnExecute = actChangeViewToDelphiVersionExecute
    end
    object actRefresh: TAction
      Hint = 'Refresh'
      ImageIndex = 7
      ImageName = 'symbol-update'
      OnExecute = actRefreshExecute
    end
  end
end
