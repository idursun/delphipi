inherited SelectFolderOptions: TSelectFolderOptions
  Left = 290
  Top = 283
  Caption = 'SelectFolderOptions'
  ClientHeight = 277
  ClientWidth = 412
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  ExplicitWidth = 412
  ExplicitHeight = 277
  PixelsPerInch = 96
  TextHeight = 13
  object grpLibrarySearchPaths: TGroupBox [0]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 406
    Height = 142
    Align = alTop
    Caption = 'Search Paths'
    TabOrder = 0
    object labelTemporary: TLabel
      AlignWithMargins = True
      Left = 10
      Top = 31
      Width = 386
      Height = 13
      Margins.Left = 8
      Margins.Top = 16
      Margins.Right = 8
      Align = alTop
      Caption = 'Grayed items are marked as temporary (right-click to toggle)'
      ExplicitWidth = 289
    end
    object listSearchPaths: TListBox
      AlignWithMargins = True
      Left = 10
      Top = 50
      Width = 360
      Height = 19
      Margins.Left = 8
      Margins.Right = 34
      Margins.Bottom = 8
      Style = lbOwnerDrawFixed
      Align = alClient
      ItemHeight = 13
      PopupMenu = PopupMenu
      TabOrder = 0
      OnClick = listSearchPathsClick
      OnDrawItem = listPathsDrawItem
      ExplicitLeft = 12
    end
    object Panel1: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 107
      Width = 386
      Height = 25
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object buttonReplaceSearch: TButton
        AlignWithMargins = True
        Left = 180
        Top = 0
        Width = 86
        Height = 25
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 8
        Margins.Bottom = 0
        Align = alRight
        Caption = 'Replace'
        Enabled = False
        TabOrder = 0
        OnClick = buttonReplaceSearchClick
      end
      object buttonAddSearch: TButton
        Left = 0
        Top = 0
        Width = 86
        Height = 25
        Align = alLeft
        Caption = 'Add'
        Enabled = False
        TabOrder = 1
        OnClick = buttonAddSearchClick
      end
      object buttonDeleteSearch: TButton
        AlignWithMargins = True
        Left = 274
        Top = 0
        Width = 86
        Height = 25
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 26
        Margins.Bottom = 0
        Align = alRight
        Caption = 'Delete'
        Enabled = False
        TabOrder = 2
        OnClick = buttonDeleteSearchClick
      end
    end
    object Panel2: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 80
      Width = 386
      Height = 21
      Margins.Left = 8
      Margins.Right = 8
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object buttonSearchFolder: TSpeedButton
        AlignWithMargins = True
        Left = 363
        Top = 0
        Width = 23
        Height = 21
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alRight
        ImageIndex = 0
        ImageName = 'folder-filled'
        Images = VirtualImageList
        OnClick = buttonSearchFolderClick
        ExplicitLeft = 366
        ExplicitTop = 3
      end
      object editSearchPath: TEdit
        Left = 0
        Top = 0
        Width = 360
        Height = 21
        Align = alClient
        TabOrder = 0
        OnChange = editSearchPathChange
        ExplicitLeft = -3
      end
    end
  end
  object grpBrowsingPaths: TGroupBox [1]
    AlignWithMargins = True
    Left = 3
    Top = 151
    Width = 406
    Height = 123
    Align = alClient
    Caption = 'Browsing Paths'
    TabOrder = 1
    object listBrowsingPaths: TListBox
      AlignWithMargins = True
      Left = 10
      Top = 31
      Width = 360
      Height = 19
      Margins.Left = 8
      Margins.Top = 16
      Margins.Right = 34
      Margins.Bottom = 8
      Style = lbOwnerDrawFixed
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = listBrowsingPathsClick
      OnDrawItem = listPathsDrawItem
    end
    object Panel3: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 61
      Width = 386
      Height = 21
      Margins.Left = 8
      Margins.Right = 8
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object buttonBrowsingFolder: TSpeedButton
        AlignWithMargins = True
        Left = 363
        Top = 0
        Width = 23
        Height = 21
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alRight
        ImageIndex = 0
        ImageName = 'folder-filled'
        Images = VirtualImageList
        OnClick = buttonBrowsingFolderClick
        ExplicitLeft = 366
        ExplicitTop = 3
      end
      object editBrowsingPath: TEdit
        Left = 0
        Top = 0
        Width = 360
        Height = 21
        Align = alClient
        TabOrder = 0
        OnChange = editBrowsingPathChange
        ExplicitLeft = -3
      end
    end
    object Panel4: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 88
      Width = 386
      Height = 25
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object buttonReplaceBrowse: TButton
        AlignWithMargins = True
        Left = 180
        Top = 0
        Width = 86
        Height = 25
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 8
        Margins.Bottom = 0
        Align = alRight
        Caption = 'Replace'
        Enabled = False
        TabOrder = 0
        OnClick = buttonReplaceBrowseClick
      end
      object buttonAddBrowse: TButton
        Left = 0
        Top = 0
        Width = 86
        Height = 25
        Align = alLeft
        Caption = 'Add'
        Enabled = False
        TabOrder = 1
        OnClick = buttonAddBrowseClick
      end
      object buttonDeleteBrowse: TButton
        AlignWithMargins = True
        Left = 274
        Top = 0
        Width = 86
        Height = 25
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 26
        Margins.Bottom = 0
        Align = alRight
        Caption = 'Delete'
        Enabled = False
        TabOrder = 2
        OnClick = buttonDeleteBrowseClick
      end
    end
  end
  inherited VirtualImageList: TVirtualImageList
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'folder'
        Name = 'folder'
      end
      item
        CollectionIndex = 1
        CollectionName = 'package'
        Name = 'package'
      end
      item
        CollectionIndex = 2
        CollectionName = 'control-tree'
        Name = 'control-tree'
      end
      item
        Name = 'list'
      end
      item
        CollectionIndex = 4
        CollectionName = 'software'
        Name = 'software'
      end
      item
        CollectionIndex = 5
        CollectionName = 'folder-add'
        Name = 'folder-add'
      end
      item
        CollectionIndex = 6
        CollectionName = 'package-add'
        Name = 'package-add'
      end
      item
        CollectionIndex = 7
        CollectionName = 'symbol-update'
        Name = 'symbol-update'
      end
      item
        CollectionIndex = 8
        CollectionName = 'control-check-box'
        Name = 'control-check-box'
      end
      item
        CollectionIndex = 9
        CollectionName = 'control-check-box-empty'
        Name = 'control-check-box-empty'
      end>
  end
  object PopupMenu: TPopupMenu
    OnPopup = PopupMenuPopup
    Left = 315
    Top = 19
    object itemTemporary: TMenuItem
      Caption = 'Temporary'
      OnClick = itemTemporaryClick
    end
  end
end
