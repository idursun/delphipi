{**
 DelphiPI (Delphi Package Installer)
 Author  : ronald siekman
 License : GNU General Public License 2.0
**}

unit PageFolderOptions;

interface

uses
  System.Classes, System.ImageList, System.Types,
  Vcl.Controls, Vcl.ImgList, Vcl.Menus, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  Vcl.BaseImageCollection, Vcl.ImageCollection,  Vcl.VirtualImageList,
  Generics.Collections, PageBase, CompilationData;

type
  TUpdate = (add, replace, delete);

  TSelectFolderOptions = class(TWizardPage)
    grpLibrarySearchPaths: TGroupBox;
    listSearchPaths: TListBox;
    grpBrowsingPaths: TGroupBox;
    listBrowsingPaths: TListBox;
    labelTemporary: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    editSearchPath: TEdit;
    buttonSearchFolder: TSpeedButton;
    Panel3: TPanel;
    buttonBrowsingFolder: TSpeedButton;
    editBrowsingPath: TEdit;
    buttonReplaceSearch: TButton;
    buttonAddSearch: TButton;
    buttonDeleteSearch: TButton;
    Panel4: TPanel;
    buttonReplaceBrowse: TButton;
    buttonAddBrowse: TButton;
    buttonDeleteBrowse: TButton;
    PopupMenu: TPopupMenu;
    itemTemporary: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure listPathsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure listSearchPathsClick(Sender: TObject);
    procedure listBrowsingPathsClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
    procedure editSearchPathChange(Sender: TObject);
    procedure editBrowsingPathChange(Sender: TObject);
    procedure buttonSearchFolderClick(Sender: TObject);
    procedure buttonBrowsingFolderClick(Sender: TObject);
    procedure buttonAddSearchClick(Sender: TObject);
    procedure buttonReplaceSearchClick(Sender: TObject);
    procedure buttonDeleteSearchClick(Sender: TObject);
    procedure buttonAddBrowseClick(Sender: TObject);
    procedure buttonReplaceBrowseClick(Sender: TObject);
    procedure buttonDeleteBrowseClick(Sender: TObject);
    procedure itemTemporaryClick(Sender: TObject);
  private
    procedure ShowCompilationDataSearchPaths;
    procedure ShowCompilationDataBrowsingPaths;
    procedure UpdateFolder(paths: TList<TFolder>; list: TListBox; state: TUpdate);
    function FolderExists(folderName: string; list: TListBox): Boolean; { BUG# 5 }
  public
    constructor Create(Owner: TComponent;
      const compilationData: TCompilationData); override;
    procedure UpdateWizardState; override;
    function CanShowPage: Boolean; override;
  end;

var
  SelectFolderOptions: TSelectFolderOptions;

implementation

uses
  Winapi.Windows, System.SysUtils, Vcl.Graphics, Vcl.Themes, gnugettext, Utils;

{$R *.dfm}

{ TSelectDelphiInstallationPage }

constructor TSelectFolderOptions.Create(Owner: TComponent;
  const compilationData: TCompilationData);
begin
  inherited;

  FCompilationData := compilationData;
end;

procedure TSelectFolderOptions.editBrowsingPathChange(Sender: TObject);
var
  hasFolder: Boolean;
begin
  hasFolder := FolderExists(editBrowsingPath.Text, listBrowsingPaths);
  buttonAddBrowse.Enabled := not(hasFolder) and (editBrowsingPath.Text <> '');
  buttonReplaceBrowse.Enabled := False;
  buttonDeleteBrowse.Enabled := False;

  if (listBrowsingPaths.ItemIndex >= 0) and
    (editBrowsingPath.Text <> '') then
  begin
    buttonReplaceBrowse.Enabled := not(hasFolder);
    buttonDeleteBrowse.Enabled := not(buttonAddBrowse.Enabled);
  end;
end;

procedure TSelectFolderOptions.editSearchPathChange(Sender: TObject);
var
  hasFolder: Boolean;
begin
  hasFolder := FolderExists(editSearchPath.Text, listSearchPaths);
  buttonAddSearch.Enabled := not(hasFolder) and (editSearchPath.Text <> '');
  buttonReplaceSearch.Enabled := False;
  buttonDeleteSearch.Enabled := False;

  if (listSearchPaths.ItemIndex >= 0) and
    (editSearchPath.Text <> '') then
  begin
    buttonReplaceSearch.Enabled := not(hasFolder);
    buttonDeleteSearch.Enabled := not(buttonAddSearch.Enabled);
  end;
end;

procedure TSelectFolderOptions.itemTemporaryClick(Sender: TObject);
var
  folder: TFolder;
begin
  itemTemporary.Checked := not(itemTemporary.Checked);
  if (listSearchPaths.ItemIndex >= 0) then
  begin
    folder := TFolder(listSearchPaths.Items.Objects[listSearchPaths.ItemIndex]);
    folder.Temporary := itemTemporary.Checked;
    listSearchPaths.Invalidate;
  end;
end;

function TSelectFolderOptions.FolderExists(folderName: string;
  list: TListBox): Boolean;
var
  I: Integer;
  folder: TFolder;
begin
  Result := False;
  if (folderName <> '') then
  begin
    I := (list.Count - 1);
    while not(Result) and (I >= 0) do
    begin
      folder := TFolder(list.Items.Objects[I]);
      Result := (LowerCase(folder.Name) = LowerCase(folderName));
      Dec(I)
    end;
  end;
end;

procedure TSelectFolderOptions.FormCreate(Sender: TObject);
begin
  inherited;

  TranslateComponent(self);
  ShowCompilationDataSearchPaths;
  ShowCompilationDataBrowsingPaths;
end;

procedure TSelectFolderOptions.FormResize(Sender: TObject);
begin
  inherited;

  grpLibrarySearchPaths.Height := Height div 2;
end;

procedure TSelectFolderOptions.FormShow(Sender: TObject);
var
  ppi: Integer;
begin
  inherited;

  ppi := Self.Monitor.PixelsPerInch;
  listSearchPaths.ItemHeight := MulDiv(16, ppi, 96);
  listBrowsingPaths.ItemHeight := listSearchPaths.ItemHeight;
end;

procedure TSelectFolderOptions.listBrowsingPathsClick(Sender: TObject);
begin
  if (listBrowsingPaths.ItemIndex >= 0) then
    editBrowsingPath.Text := listBrowsingPaths.Items[listBrowsingPaths.ItemIndex];
  editBrowsingPathChange(Self);
end;

procedure TSelectFolderOptions.listSearchPathsClick(Sender: TObject);
begin
  if (listSearchPaths.ItemIndex >= 0) then
    editSearchPath.Text := listSearchPaths.Items[listSearchPaths.ItemIndex];
  editSearchPathChange(Self)
end;

procedure TSelectFolderOptions.listPathsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    if TFolder((Control as TListBox).Items.Objects[Index]).Temporary then
    begin
      if (odSelected in State) then
      begin
        Brush.Color := StyleServices.GetSystemColor(clGrayText);
        FillRect(Rect);
      end
      else
        Font.Color := StyleServices.GetSystemColor(clGrayText);
    end
    else
    begin
      if (odSelected in State) then
      begin
        Brush.Color := StyleServices.GetSystemColor(clHighlight);
        Font.Color := StyleServices.GetSystemColor(clHighlightText);
        FillRect(Rect);
      end
    end;

    TextOut(Rect.Left, Rect.Top, (Control as TListBox).Items[Index]);
  end;
end;

procedure TSelectFolderOptions.PopupMenuPopup(Sender: TObject);
var
  folder: TFolder;
begin
  if (listSearchPaths.ItemIndex >= 0) then
  begin
    folder := TFolder(listSearchPaths.Items.Objects[listSearchPaths.ItemIndex]);
    itemTemporary.Checked := folder.Temporary;
  end;
end;

procedure TSelectFolderOptions.buttonAddBrowseClick(Sender: TObject);
begin
  UpdateFolder(fCompilationData.BrowsingPaths, listBrowsingPaths, TUpdate.add);
end;

procedure TSelectFolderOptions.buttonAddSearchClick(Sender: TObject);
begin
  UpdateFolder(fCompilationData.SearchPaths, listSearchPaths, TUpdate.add);
end;

procedure TSelectFolderOptions.buttonBrowsingFolderClick(Sender: TObject);
begin
  SelectFolder(editBrowsingPath);
end;

procedure TSelectFolderOptions.buttonDeleteBrowseClick(Sender: TObject);
begin
  UpdateFolder(fCompilationData.BrowsingPaths, listBrowsingPaths, TUpdate.delete);
end;

procedure TSelectFolderOptions.buttonDeleteSearchClick(Sender: TObject);
begin
  UpdateFolder(fCompilationData.SearchPaths, listSearchPaths, TUpdate.delete);
end;

procedure TSelectFolderOptions.buttonReplaceBrowseClick(Sender: TObject);
begin
  UpdateFolder(fCompilationData.BrowsingPaths, listBrowsingPaths, TUpdate.replace);
end;

procedure TSelectFolderOptions.buttonReplaceSearchClick(Sender: TObject);
begin
  UpdateFolder(fCompilationData.SearchPaths, listSearchPaths, TUpdate.replace);
end;

procedure TSelectFolderOptions.buttonSearchFolderClick(Sender: TObject);
begin
  SelectFolder(editSearchPath);
end;

function TSelectFolderOptions.CanShowPage: Boolean;
begin
  Result := True;
end;

procedure TSelectFolderOptions.ShowCompilationDataBrowsingPaths;
var
  folder: TFolder;
begin
  for folder in fCompilationData.BrowsingPaths do
    listBrowsingPaths.AddItem(folder.Name, folder);

  if (listBrowsingPaths.Count > 0) then
  begin
    listBrowsingPaths.ItemIndex := 0;
    editBrowsingPath.Text := listBrowsingPaths.Items[0];
  end;
end;

procedure TSelectFolderOptions.ShowCompilationDataSearchPaths;
var
  folder: TFolder;
begin
  for folder in fCompilationData.SearchPaths do
    listSearchPaths.AddItem(folder.Name, folder);

  if (listSearchPaths.Count > 0) then
  begin
    listSearchPaths.ItemIndex := 0;
    editSearchPath.Text := listSearchPaths.Items[0];
  end;
end;

procedure TSelectFolderOptions.UpdateFolder(paths: TList<TFolder>;
  list: TListBox; state: TUpdate);
var
  folder: TFolder;
  folderName: string;
  index: Integer;
begin
  if (list = listSearchPaths) then folderName := editSearchPath.Text
  else folderName := editBrowsingPath.Text;

  case state of
    Add:
      begin
        folder := TFolder.Create(folderName);
        paths.Add(folder);
        list.AddItem(folder.Name, folder);
        list.ItemIndex := (list.Count - 1);
      end;
    Replace:
      begin
        if (list.ItemIndex >= 0) then
        begin
          folder := TFolder(list.Items.Objects[list.ItemIndex]);
          folder.Name := folderName;
          list.Items[list.ItemIndex] := folder.Name;
        end;
      end;
    Delete:
      begin
        if (list.ItemIndex >= 0) then
        begin
          folder := TFolder(list.Items.Objects[list.ItemIndex]);
          list.Items.Delete(list.ItemIndex);
          if (list = listSearchPaths) then editSearchPath.Text := ''
          else editBrowsingPath.Text := '';
          index := paths.IndexOf(folder);
          if (index <> -1) then
          begin
            folder.Free;
            paths.Delete(index);
          end;
        end;
      end;
  end;

  if (list = listSearchPaths) then editSearchPathChange(Self)
  else editBrowsingPathChange(Self);
end;

procedure TSelectFolderOptions.UpdateWizardState;
begin
  inherited;

  wizard.SetHeader(_('Add optional Search and Browsing Paths'));
  wizard.SetDescription(_('Please add Search Paths that will affect the compilation'));
end;

end.
