inherited SummaryPage: TSummaryPage
  Left = 328
  Top = 271
  Caption = 'SummaryPage'
  ClientHeight = 234
  ClientWidth = 412
  OnCreate = FormCreate
  ExplicitWidth = 412
  ExplicitHeight = 234
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 192
    Width = 310
    Height = 34
    Anchors = [akLeft, akRight, akBottom]
    AutoSize = False
    Caption = 
      'Would you like to save this installation as a script so that you' +
      ' can automate installation of these packages?'
    WordWrap = True
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Summary:'
  end
  object btnSave: TButton [2]
    Left = 318
    Top = 187
    Width = 86
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save'
    TabOrder = 0
    OnClick = btnSaveClick
  end
  object edtSummary: TMemo [3]
    Left = 8
    Top = 27
    Width = 396
    Height = 152
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    ExplicitHeight = 150
  end
  inherited VirtualImageList: TVirtualImageList
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'folder'
        Name = 'folder'
      end
      item
        CollectionIndex = 1
        CollectionName = 'package'
        Name = 'package'
      end
      item
        CollectionIndex = 2
        CollectionName = 'control-tree'
        Name = 'control-tree'
      end
      item
        CollectionIndex = 3
        CollectionName = 'control-tree-list'
        Name = 'control-tree-list'
      end
      item
        CollectionIndex = 4
        CollectionName = 'software'
        Name = 'software'
      end
      item
        CollectionIndex = 5
        CollectionName = 'folder-add'
        Name = 'folder-add'
      end
      item
        CollectionIndex = 6
        CollectionName = 'package-add'
        Name = 'package-add'
      end
      item
        CollectionIndex = 7
        CollectionName = 'symbol-update'
        Name = 'symbol-update'
      end
      item
        CollectionIndex = 8
        CollectionName = 'control-check-box'
        Name = 'control-check-box'
      end
      item
        CollectionIndex = 9
        CollectionName = 'control-check-box-empty'
        Name = 'control-check-box-empty'
      end>
  end
end
