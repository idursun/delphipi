inherited SelectCompilerOptions: TSelectCompilerOptions
  Left = 290
  Top = 283
  Caption = 'SelectCompilerOptions'
  ClientHeight = 209
  ClientWidth = 426
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 426
  ExplicitHeight = 209
  PixelsPerInch = 96
  TextHeight = 13
  object grpOutputFolders: TGroupBox [0]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 420
    Height = 198
    Align = alTop
    Caption = 'Compiler Options'
    TabOrder = 0
    DesignSize = (
      420
      198)
    object btnBPLBrowse: TButton
      Left = 460
      Top = 22
      Width = 28
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 0
    end
    object btnDCPBrowse: TButton
      Left = 460
      Top = 51
      Width = 28
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 1
    end
    object btnDCUBrowse: TButton
      Left = 460
      Top = 78
      Width = 28
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 2
    end
    object panelBPL: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 31
      Width = 400
      Height = 21
      Margins.Left = 8
      Margins.Top = 16
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object buttonBPL: TSpeedButton
        AlignWithMargins = True
        Left = 377
        Top = 0
        Width = 23
        Height = 21
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alRight
        ImageIndex = 0
        ImageName = 'folder-filled'
        Images = VirtualImageList
        OnClick = buttonBPLClick
        ExplicitLeft = 380
        ExplicitTop = 3
      end
      object lblBPLOutputFolder: TLabel
        Left = 0
        Top = 0
        Width = 120
        Height = 21
        Align = alLeft
        AutoSize = False
        Caption = 'BPL Output Folder:'
        Layout = tlCenter
      end
      object edtBPL: TEdit
        Left = 120
        Top = 0
        Width = 254
        Height = 21
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 113
        ExplicitWidth = 261
      end
    end
    object panelDirectives: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 127
      Width = 400
      Height = 21
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object labelDirectives: TLabel
        Left = 0
        Top = 0
        Width = 120
        Height = 21
        Align = alLeft
        AutoSize = False
        Caption = 'Compiler Directives:'
        Layout = tlCenter
      end
      object edtConditionals: TEdit
        Left = 120
        Top = 0
        Width = 280
        Height = 21
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 113
        ExplicitWidth = 287
      end
    end
    object panelDCU: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 95
      Width = 400
      Height = 21
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 5
      object buttonDCU: TSpeedButton
        AlignWithMargins = True
        Left = 377
        Top = 0
        Width = 23
        Height = 21
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alRight
        ImageIndex = 0
        ImageName = 'folder-filled'
        Images = VirtualImageList
        OnClick = buttonDCUClick
        ExplicitLeft = 380
        ExplicitTop = 3
      end
      object lblDCU: TLabel
        Left = 0
        Top = 0
        Width = 120
        Height = 21
        Align = alLeft
        AutoSize = False
        Caption = 'DCU Output Folder:'
        Layout = tlCenter
      end
      object edtDCU: TEdit
        Left = 120
        Top = 0
        Width = 254
        Height = 21
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 113
        ExplicitWidth = 261
      end
    end
    object panelDCP: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 63
      Width = 400
      Height = 21
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 6
      object buttonDCP: TSpeedButton
        AlignWithMargins = True
        Left = 377
        Top = 0
        Width = 23
        Height = 21
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alRight
        ImageIndex = 0
        ImageName = 'folder-filled'
        Images = VirtualImageList
        OnClick = buttonDCPClick
        ExplicitLeft = 380
        ExplicitTop = 3
      end
      object lblDCP: TLabel
        Left = 0
        Top = 0
        Width = 120
        Height = 21
        Align = alLeft
        AutoSize = False
        Caption = 'DCP Output Folder:'
        Layout = tlCenter
      end
      object edtDCP: TEdit
        Left = 120
        Top = 0
        Width = 254
        Height = 21
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 113
        ExplicitWidth = 261
      end
    end
    object Panel1: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 159
      Width = 400
      Height = 21
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 7
      ExplicitTop = 204
      object labelBuildWin64: TLabel
        Left = 0
        Top = 0
        Width = 120
        Height = 21
        Align = alLeft
        AutoSize = False
        Caption = 'Build win64 Packages:'
        Layout = tlCenter
      end
      object chkBuildWin64: TCheckBox
        Left = 120
        Top = 0
        Width = 280
        Height = 21
        Align = alClient
        Checked = True
        State = cbChecked
        TabOrder = 0
        ExplicitLeft = 119
        ExplicitTop = -5
        ExplicitWidth = 24
      end
    end
  end
  inherited VirtualImageList: TVirtualImageList
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'folder'
        Name = 'folder'
      end
      item
        CollectionIndex = 1
        CollectionName = 'package'
        Name = 'package'
      end
      item
        CollectionIndex = 2
        CollectionName = 'control-tree'
        Name = 'control-tree'
      end
      item
        CollectionIndex = 3
        CollectionName = 'control-tree-list'
        Name = 'control-tree-list'
      end
      item
        CollectionIndex = 4
        CollectionName = 'software'
        Name = 'software'
      end
      item
        CollectionIndex = 5
        CollectionName = 'folder-add'
        Name = 'folder-add'
      end
      item
        CollectionIndex = 6
        CollectionName = 'package-add'
        Name = 'package-add'
      end
      item
        CollectionIndex = 7
        CollectionName = 'symbol-update'
        Name = 'symbol-update'
      end
      item
        CollectionIndex = 8
        CollectionName = 'control-check-box'
        Name = 'control-check-box'
      end
      item
        CollectionIndex = 9
        CollectionName = 'control-check-box-empty'
        Name = 'control-check-box-empty'
      end>
  end
end
