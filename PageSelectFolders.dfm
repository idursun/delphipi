inherited SelectFoldersPage: TSelectFoldersPage
  Left = 346
  Top = 221
  Caption = 'SelectFoldersPage'
  ClientHeight = 298
  ClientWidth = 480
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 480
  ExplicitHeight = 298
  PixelsPerInch = 96
  TextHeight = 13
  object grpPackagePattern: TGroupBox [0]
    AlignWithMargins = True
    Left = 3
    Top = 183
    Width = 474
    Height = 111
    Align = alTop
    Caption = 'Pattern to select package files '
    TabOrder = 2
    object labelPatternDesc: TLabel
      AlignWithMargins = True
      Left = 10
      Top = 31
      Width = 454
      Height = 26
      Margins.Left = 8
      Margins.Top = 16
      Margins.Right = 8
      Align = alTop
      Caption = 
        'Specify a pattern that matches for the package files that are su' +
        'itable for your delphi installation. ie: *d7.dpk for Delphi 7'
      WordWrap = True
      ExplicitWidth = 408
    end
    object Panel2: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 68
      Width = 454
      Height = 21
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object labelPattern: TLabel
        Left = 0
        Top = 0
        Width = 129
        Height = 21
        Align = alLeft
        AutoSize = False
        Caption = 'Package File Pattern'
        Layout = tlCenter
      end
      object cbPattern: TComboBox
        Left = 129
        Top = 0
        Width = 325
        Height = 21
        Align = alClient
        TabOrder = 0
        Text = '*.dpk'
        ExplicitLeft = 135
        ExplicitTop = -5
      end
    end
  end
  object grpBaseFolder: TGroupBox [1]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 474
    Height = 94
    Align = alTop
    Caption = 'Select Base Folder where the packages are'
    TabOrder = 0
    DesignSize = (
      474
      94)
    object labelBaseFolder: TLabel
      AlignWithMargins = True
      Left = 10
      Top = 31
      Width = 454
      Height = 13
      Margins.Left = 8
      Margins.Top = 16
      Margins.Right = 8
      Align = alTop
      Caption = 'Base Folder'
      ExplicitWidth = 56
    end
    object btnSelectFolder: TButton
      Left = 628
      Top = 35
      Width = 32
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 0
      WordWrap = True
      OnClick = btnSelectFolderClick
    end
    object panelBaseFolder: TPanel
      AlignWithMargins = True
      Left = 10
      Top = 50
      Width = 454
      Height = 21
      Margins.Left = 8
      Margins.Right = 8
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object buttonBaseFolder: TSpeedButton
        AlignWithMargins = True
        Left = 431
        Top = 0
        Width = 23
        Height = 21
        Margins.Left = 8
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alRight
        ImageIndex = 0
        ImageName = 'folder-filled'
        Images = VirtualImageList
        OnClick = buttonBaseFolderClick
        ExplicitLeft = 434
        ExplicitTop = -3
      end
      object edtBaseFolder: TEdit
        Left = 0
        Top = 0
        Width = 423
        Height = 21
        Align = alClient
        TabOrder = 0
        OnChange = edtBaseFolderChange
        ExplicitLeft = -3
        ExplicitWidth = 415
      end
    end
  end
  object grpDelphiVersion: TGroupBox [2]
    AlignWithMargins = True
    Left = 3
    Top = 103
    Width = 474
    Height = 74
    Align = alTop
    Caption = 'Installed Delphi Versions'
    TabOrder = 1
    object cbDelphiVersions: TComboBox
      AlignWithMargins = True
      Left = 10
      Top = 31
      Width = 454
      Height = 21
      Margins.Left = 8
      Margins.Top = 16
      Margins.Right = 8
      Margins.Bottom = 8
      Align = alTop
      Style = csDropDownList
      TabOrder = 0
      OnChange = cbDelphiVersionsChange
    end
  end
end
