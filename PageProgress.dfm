inherited ProgressPage: TProgressPage
  Left = 299
  Top = 280
  Caption = 'ProgressPage'
  ClientHeight = 232
  ClientWidth = 412
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 412
  ExplicitHeight = 232
  TextHeight = 13
  object GroupBox1: TGroupBox [0]
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 406
    Height = 68
    Align = alTop
    Caption = 'Overall Progress'
    TabOrder = 0
    DesignSize = (
      406
      68)
    object Label1: TLabel
      Left = 12
      Top = 18
      Width = 44
      Height = 13
      Caption = 'Package:'
    end
    object lblPackage: TLabel
      Left = 62
      Top = 18
      Width = 61
      Height = 13
      Caption = 'lblPackage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCurrentPackageNo: TLabel
      Left = 314
      Top = 18
      Width = 15
      Height = 13
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = '     '
    end
    object ProgressBar: TProgressBar
      Left = 12
      Top = 37
      Width = 296
      Height = 20
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 314
      Top = 37
      Width = 86
      Height = 22
      Anchors = [akTop, akRight]
      Caption = 'Cancel'
      TabOrder = 1
      OnClick = btnCancelClick
    end
  end
  object memo: TRichEdit [1]
    AlignWithMargins = True
    Left = 3
    Top = 77
    Width = 406
    Height = 119
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object chkShowFullLog: TCheckBox [2]
    AlignWithMargins = True
    Left = 3
    Top = 207
    Width = 406
    Height = 17
    Margins.Top = 8
    Margins.Bottom = 8
    Align = alBottom
    Caption = 'Show Full Log'
    TabOrder = 2
    OnClick = chkShowFullLogClick
  end
  inherited VirtualImageList: TVirtualImageList
    Images = <
      item
        CollectionIndex = 0
        CollectionName = 'folder'
        Name = 'folder'
      end
      item
        CollectionIndex = 1
        CollectionName = 'package'
        Name = 'package'
      end
      item
        CollectionIndex = 2
        CollectionName = 'control-tree'
        Name = 'control-tree'
      end
      item
        CollectionIndex = 3
        CollectionName = 'control-tree-list'
        Name = 'control-tree-list'
      end
      item
        CollectionIndex = 4
        CollectionName = 'software'
        Name = 'software'
      end
      item
        Name = 'folder-add'
      end
      item
        Name = 'package-add'
      end
      item
        CollectionIndex = 7
        CollectionName = 'symbol-update'
        Name = 'symbol-update'
      end
      item
        CollectionIndex = 8
        CollectionName = 'control-check-box'
        Name = 'control-check-box'
      end
      item
        CollectionIndex = 9
        CollectionName = 'control-check-box-empty'
        Name = 'control-check-box-empty'
      end>
  end
end
